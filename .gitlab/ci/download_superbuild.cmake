cmake_minimum_required(VERSION 3.12)

# In order to use a new superbuild, please copy the item from the date stamped
# directory into the `keep` directory. This ensure that new versions will not
# be removed by the uploader script and preserve them for debugging in the
# future.

set(data_host "https://data.kitware.com")

# Determine the tarball to download.
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "vs2019")
  # 20200831
  set(file_item "5f4d0fe29014a6d84e9e8df5")
  set(file_hash "60e4e34018ce40bd29a50ace4e589e66e42fb04bcad9c35e6f2e9e237b3043a188861302dc4d52ea0016b8314a96ec500c4c7f18cc89449cc5caf7959b215d86")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos")
  # 20200831
  set(file_item "5f4d101b9014a6d84e9e8e1a")
  set(file_hash "930e983830e85dcb6d9c4c021bf209ebda005aa8b470f0d4729dacdddd367ce09e8f1d89635bc182e00225d9a116759c9cf9bf910252755ede3fd04097a9fd7a")
else ()
  message(FATAL_ERROR
    "Unknown build to use for the superbuild")
endif ()

# Ensure we have a hash to verify.
if (NOT DEFINED file_item OR NOT DEFINED file_hash)
  message(FATAL_ERROR
    "Unknown file and hash for the superbuild")
endif ()

# Download the file.
file(DOWNLOAD
  "${data_host}/api/v1/item/${file_item}/download"
  ".gitlab/superbuild.tar.gz"
  STATUS download_status
  EXPECTED_HASH "SHA512=${file_hash}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download superbuild.tar.gz: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xf ".gitlab/superbuild.tar.gz"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract superbuild.tar.gz: ${err}")
endif ()
