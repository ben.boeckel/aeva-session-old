//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef vtk_aeva_ext_vtkProportionalEditWidget_h
#define vtk_aeva_ext_vtkProportionalEditWidget_h

#include "vtk/aeva/ext/AEVAExtModule.h" // For export macro
#include "vtkAbstractWidget.h"

class vtkProportionalEditRepresentation;

/**
 * @class   vtkProportionalEditWidget
 * @brief   3D widget for manipulating an geometry ProportionalEdit
 *
 * This 3D widget an origin point, an end point, and a radius of influence.

 * To use this widget, you pair it with a vtkProportionalEditRepresentation.
 *
*/
class AEVAEXT_EXPORT vtkProportionalEditWidget : public vtkAbstractWidget
{
public:
  /**
   * Instantiate the object.
   */
  static vtkProportionalEditWidget* New();

  //@{
  /**
   * Standard vtkObject methods
   */
  vtkTypeMacro(vtkProportionalEditWidget, vtkAbstractWidget);
  void PrintSelf(ostream& os, vtkIndent indent) override;
  //@}

  /**
   * Specify an instance of vtkWidgetRepresentation used to represent this
   * widget in the scene. Note that the representation is a subclass of vtkProp
   * so it can be added to the renderer independent of the widget.
   */
  void SetRepresentation(vtkProportionalEditRepresentation* rep);

  /// Control widget interactivity, allowing users to interact with the camera or other widgets.
  ///
  /// The camera is unobserved when the widget is disabled.
  void SetEnabled(int enabling) override;

  /**
   * Return the representation as a vtkProportionalEditRepresentation.
   */
  vtkProportionalEditRepresentation* GetProportionalEditRepresentation()
  {
    return reinterpret_cast<vtkProportionalEditRepresentation*>(this->WidgetRep);
  }

  /**
   * Create the default widget representation if one is not set.
   */
  void CreateDefaultRepresentation() override;

protected:
  vtkProportionalEditWidget();
  ~vtkProportionalEditWidget() override;

  // Manage the state of the widget
  int WidgetState;
  enum _WidgetState
  {
    Start = 0,
    Active
  };

  // These methods handle events
  static void SelectAction(vtkAbstractWidget*);
  static void TranslateAction(vtkAbstractWidget*);
  static void ScaleAction(vtkAbstractWidget*);
  static void EndSelectAction(vtkAbstractWidget*);
  static void MoveAction(vtkAbstractWidget*);
  static void MoveConeAction(vtkAbstractWidget*);

  /**
   * Update the cursor shape based on the interaction state. Returns 1
   * if the cursor shape requested is different from the existing one.
   */
  int UpdateCursorShape(int interactionState);

private:
  vtkProportionalEditWidget(const vtkProportionalEditWidget&) = delete;
  void operator=(const vtkProportionalEditWidget&) = delete;
};

#endif
