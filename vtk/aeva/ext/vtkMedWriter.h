//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef vtk_aeva_ext_vtkMedWriter_h
#define vtk_aeva_ext_vtkMedWriter_h

#include "vtk/aeva/ext/AEVAExtModule.h"
#include "vtkMultiBlockDataSetAlgorithm.h"

#include <string>

class vtkMultiBlockDataSet;

// vtkMedWriter writes a mesh to a med file when provided with a valid
// vtkMultiBlockDataSet of vtkUnstructuredGrids
// per cell type as well as all the groups separately
// These groups come in a vtkMultiBlockDataSet as vtkUnstructuredGrids.
class AEVAEXT_EXPORT vtkMedWriter : public vtkMultiBlockDataSetAlgorithm
{
public:
  static vtkMedWriter* New();
  vtkTypeMacro(vtkMedWriter, vtkMultiBlockDataSetAlgorithm);

public:
  void PrintSelf(ostream& os, vtkIndent indent) override;

  //@{
  /**
    * The file to write too
    */
  vtkSetMacro(FileName, std::string);
  vtkGetMacro(FileName, std::string);
  //@}

  //@{
  /**
    * The name of the mesh within the file
    */
  vtkSetMacro(MeshName, std::string);
  vtkGetMacro(MeshName, std::string);
  //@}

protected:
  vtkMedWriter() { SetNumberOfOutputPorts(0); }

  int RequestData(vtkInformation* request,
    vtkInformationVector** inputVec,
    vtkInformationVector* outputVec) override;

  int FillInputPortInformation(int port, vtkInformation* info) override;

  std::string FileName = "";
  std::string MeshName = "MESH";

private:
  vtkMedWriter(const vtkMedWriter&) = delete;
  void operator=(const vtkMedWriter&) = delete;
};

#endif
