set(classes
  vtkGlobalIdBooleans
  vtkMedReader
  vtkMedWriter
  vtkMedHelper
  vtkProportionalEditElements
  vtkProportionalEditFilter
  vtkProportionalEditRepresentation
  vtkProportionalEditWidget
  vtkSideSetsToScalars
)

vtk_module_add_module(VTK::AEVAExt
  CLASSES ${classes}
  HEADERS_SUBDIR "vtk/aeva/ext")

target_include_directories(AEVAExt
  PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
    $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}>
)
