// Copyright (c) Kitware, Inc. All rights reserved. See license.md for details.
#include "smtk/io/Logger.h"

#include "smtk/session/aeva/CellSelection.h"
#include "smtk/session/aeva/Resource.h"

namespace smtk
{
namespace session
{
namespace aeva
{

Resource::Resource(const smtk::common::UUID& uid, smtk::resource::ManagerPtr const& manager)
  : smtk::resource::DerivedFrom<Resource, smtk::model::Resource>(uid, manager)
{
}

Resource::Resource(smtk::resource::ManagerPtr const& manager)
  : smtk::resource::DerivedFrom<Resource, smtk::model::Resource>(manager)
{
}

Resource::~Resource() = default;

void Resource::setSession(const Session::Ptr& session)
{
  m_session = session->shared_from_this();
  this->registerSession(m_session);
}

smtk::resource::ComponentPtr Resource::find(const smtk::common::UUID& compId) const
{
  auto cs = CellSelection::instance();
  if (cs && cs->id() == compId)
  {
    return cs;
  }
  return this->Superclass::find(compId);
}

} // namespace aeva
} // namespace session
} // namespace smtk
