<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "proximity feature" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="boolean unite" Label="Union" BaseType="operation">

      <BriefDescription>Perform a set-based boolean union.</BriefDescription>
      <DetailedDescription>
        The output workpiece will contain every primitive
        that appears in either the input workpiece or the tools.
      </DetailedDescription>
      <AssociationsDef Name="workpieces" NumberOfRequiredValues="2">
        <BriefDescription>Side sets to be united.</BriefDescription>
          <!-- TODO: Accepts should include only side sets -->
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="any"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Void Name="keep inputs" Optional="true" IsEnabledByDefault="true">
          <BriefDescription>Should the workpiece and tool objects survive the operation?</BriefDescription>
        </Void>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(boolean unite)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" Optional="true" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
