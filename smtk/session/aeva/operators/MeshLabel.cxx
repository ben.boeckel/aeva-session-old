//=============================================================================
// Copyright (c) Kitware, Inc.
// All rights reserved.
// See LICENSE.txt for details.
//
// This software is distributed WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#include "smtk/io/Logger.h"

#include "smtk/session/aeva/operators/MeshLabel.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/StringItem.h"

#include "smtk/session/aeva/MeshLabel_xml.h"

using smtk::common::UUID;

namespace smtk
{
namespace session
{
namespace aeva
{

MeshLabel::Result MeshLabel::operateInternal()
{
  return this->createResult(smtk::operation::Operation::Outcome::FAILED);
}

const char* MeshLabel::xmlDescription() const
{
  return MeshLabel_xml;
}

}
}
}