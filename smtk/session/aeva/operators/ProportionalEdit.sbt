<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "proportional edit" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="proportional edit" Label="Proportional Edit" BaseType="operation">
      <BriefDescription>Deform a surface using vtk proportional editing filter</BriefDescription>
      <DetailedDescription>
        Deform a surface using vtk proportional editing filter.
      </DetailedDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1">
        <BriefDescription>The input surface to deform</BriefDescription>
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Group Name="influence region" Label="Influence Region" NumberOfGroups="1">
          <ItemDefinitions>
            <Double Name="influence radius" Label="Radius of Influence">
              <BriefDescription>Radius of influence</BriefDescription>
              <DefaultValue>5</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Double>

            <Double Name="anchor point" Label="Center Point" NumberOfRequiredValues="3">
              <BriefDescription>Center point coordinate of the influence region</BriefDescription>
              <DefaultValue>0., 0., 0.</DefaultValue>
              <ComponentLabels>
                <Label>X</Label>
                <Label>Y</Label>
                <Label>Z</Label>
              </ComponentLabels>
            </Double>

            <Double Name="displacement point" Label="Displacement Point" NumberOfRequiredValues="3">
              <BriefDescription> Destination point for the displacement</BriefDescription>
              <DefaultValue>20., 0., 0.</DefaultValue>
              <ComponentLabels>
                <Label>X</Label>
                <Label>Y</Label>
                <Label>Z</Label>
              </ComponentLabels>
            </Double>

            <Double Name="projection direction" Label="Direction of Projection" NumberOfRequiredValues="3" Optional="true" IsEnabledByDefault="false">
              <BriefDescription>Direction of projection in 3D</BriefDescription>
              <DefaultValue>0., 0., 1.</DefaultValue>
              <ComponentLabels>
                <Label>X</Label>
                <Label>Y</Label>
                <Label>Z</Label>
              </ComponentLabels>
            </Double>
          </ItemDefinitions>
        </Group>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(proportional edit)" BaseType="result"/>
  </Definitions>
  <Views>
    <View
      Type="Operation"
      Title="ProportionalEdit Widget"
      TopLevel="true"
      FilterByAdvanceLevel="false"
      FilterByCategoryMode="false"
    >
      <InstancedAttributes>
        <Att Type="proportional edit" Name="proportional edit"
        >
          <ItemViews>
            <View Item="influence region"
              Type="ProportionalEditSphere"
              InfluenceRadius="influence radius"
              AnchorPoint="anchor point"
              Displacement="displacement point"
              ShowControls="true"/>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
