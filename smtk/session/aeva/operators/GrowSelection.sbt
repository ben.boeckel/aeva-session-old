<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "proximity feature" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="grow selection" Label="Grow Selection" BaseType="operation">

      <BriefDescription>Grow a cell selection to include adjacent cells.</BriefDescription>
      <DetailedDescription>
        Grow a cell selection to include adjacent cells.
        The growth stops when a termination condition is met.
        Currently, growth stops when a dihedral angle between adjacent faces is above
        a threshold, indicating a sharp feature exists.
      </DetailedDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1">
        <BriefDescription>The input selection that will serve as a seed for growth.</BriefDescription>
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Double Name="angle" Label="Angle threshold" NumberOfRequiredValues="1" Units="degrees">
          <BriefDescription>The maximum allowed dihedral angle between adjacent surface cells.</BriefDescription>
          <DefaultValue>30.</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.</Min>
            <Min Inclusive="false">180.</Min>
          </RangeInfo>
        </Double>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(grow selection)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
