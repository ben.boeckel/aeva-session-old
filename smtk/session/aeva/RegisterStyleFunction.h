//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_RegisterStyleFunction_h
#define smtk_session_aeva_RegisterStyleFunction_h

#include "smtk/session/aeva/Exports.h"

#include "smtk/extension/paraview/server/vtkSMTKRepresentationStyleGenerator.h"
#include "smtk/extension/paraview/server/vtkSMTKResourceRepresentation.h"

namespace smtk
{
namespace session
{
namespace aeva
{

class SMTKAEVASESSION_EXPORT RegisterStyleFunction
  : public vtkSMTKRepresentationStyleSupplier<RegisterStyleFunction>
{
public:
  bool valid(const smtk::resource::ResourcePtr& in) const override { return true; }

  /// Show the selection from smtk, but show CellSelections in a different color.
  StyleFromSelectionFunction operator()(const smtk::resource::ResourcePtr& in) override;
};
}
}
}

#endif // smtk_session_aeva_RegisterStyleFunction_h
