//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_aeva_pqSMTKProportionalEditItemWidget_h
#define smtk_session_aeva_pqSMTKProportionalEditItemWidget_h

#include "smtk/extension/paraview/widgets/pqSMTKAttributeItemWidget.h"
class pqSMTKProportionalEditItemWidget : public pqSMTKAttributeItemWidget
{
  Q_OBJECT
public:
  pqSMTKProportionalEditItemWidget(const smtk::extension::qtAttributeItemInfo& info,
    Qt::Orientation orient = Qt::Horizontal);
  virtual ~pqSMTKProportionalEditItemWidget();

  /// Create an instance of the widget that allows users to define a sphere.
  static qtItem* createSphereItemWidget(const qtAttributeItemInfo& info);
  /// Create an instance of the widget that allows users to define a cylinder.
  static qtItem* createCylinderItemWidget(const qtAttributeItemInfo& info);

  bool createProxyAndWidget(vtkSMProxy*& proxy, pqInteractivePropertyWidget*& widget) override;

protected slots:
  /// Retrieve property values from ParaView proxy and store them in the attribute's Item.
  void updateItemFromWidgetInternal() override;
  /// Retrieve property values from the attribute's Item and update the ParaView proxy.
  void updateWidgetFromItemInternal() override;

public slots:
  bool setProjectionEnabled(bool);

protected:
  /// Describe how an attribute's items specify a sphere or cylinder.
  enum class ItemBindings
  {
    /// 1 item with 1 value, 2 items with 3 values (r, x0, y0, z0, x1, y1, z1).
    SphereRadiusPointDisplacement,
    /// 1 item with 1 value, 3 item with 3 values (r, x0, y0, z0, x1, y1, z1, x2, y2, z2).
    CylinderRadiusPointDisplacementDirection,
    /// No consistent set of items detected.
    Invalid
  };

  bool fetchProportionalEditItems(ItemBindings& binding,
    std::vector<smtk::attribute::DoubleItemPtr>& items);

  bool m_projection;
};

#endif //smtk_session_aeva_pqSMTKPropEditItemWidget_h
