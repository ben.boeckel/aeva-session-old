//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_aeva_pqProportionalEditPropertyWidget_h
#define smtk_session_aeva_pqProportionalEditPropertyWidget_h

#include "pqInteractivePropertyWidget.h"

class pqProportionalEditPropertyWidget : public pqInteractivePropertyWidget
{
  Q_OBJECT
  using Superclass = pqInteractivePropertyWidget;

public:
  pqProportionalEditPropertyWidget(vtkSMProxy* proxy,
    vtkSMPropertyGroup* smgroup,
    QWidget* parent = nullptr);
  ~pqProportionalEditPropertyWidget() override;

public slots:
  void pickPoint1(double wx, double wy, double wz); //center point
  void pickPoint2(double wx, double wy, double wz); //displacement point
  void pickPoint3(double wx, double wy, double wz); //projection point
  /// Set the influence region as an infinite long cylinder (when true).
  void setProjectionEnabled(bool);

protected slots:
  void placeWidget() override;

protected:
  class Internals;
  Internals* m_p;

private:
  Q_DISABLE_COPY(pqProportionalEditPropertyWidget);
};

#endif //smtk_session_aeva_pqProportionalEditPropertyWidget_h
