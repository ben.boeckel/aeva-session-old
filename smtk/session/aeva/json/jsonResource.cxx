//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/aeva/json/jsonResource.h"

#include "smtk/session/aeva/Resource.h"

#include "smtk/model/json/jsonResource.h"

// Define how aeva resources are serialized.
namespace smtk
{
namespace session
{
namespace aeva
{
using json = nlohmann::json;
void to_json(json& j, const smtk::session::aeva::Resource::Ptr& resource)
{
  smtk::model::to_json(j, std::static_pointer_cast<smtk::model::Resource>(resource));
}

void from_json(const json& j, smtk::session::aeva::Resource::Ptr& resource)
{
  if (resource == nullptr)
  {
    resource = smtk::session::aeva::Resource::create();
  }
  auto temp = std::static_pointer_cast<smtk::model::Resource>(resource);
  smtk::model::from_json(j, temp);
}
}
}
}
